<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login',['uses'=> 'UsersController@userAuth']);
Route::get('users',['uses'=> 'UsersController@users']);

//Lista todos los datos
Route::get('Categorias', ['uses'=>'CategoriasController@index']);
//Agrega un nuevo registro
Route::post('Categorias', ['uses'=>'CategoriasController@add']);
//Elimina un registro
Route::delete('Categorias/{id}', ['uses'=>'CategoriasController@delete']);
//Monta un determinado registro con el id
Route::get('Categorias/{id}',['uses'=>'CategoriasController@edit']);
//Actualiza un registro
Route::put('Categorias/{id}',['uses'=>'CategoriasController@update']);

/**
 * SUBCATEGORIAS
 */
//Lista todos los datos
Route::get('Subcategorias', ['uses'=>'SubcategoriasController@index']);
//Agrega un nuevo registro
Route::post('Subcategorias', ['uses'=>'SubcategoriasController@add']);
//Elimina un registro
Route::delete('Subcategorias/{id}', ['uses'=>'SubcategoriasController@delete']);
//Monta un determinado registro con el id
Route::get('Subcategorias/{id}',['uses'=>'SubcategoriasController@edit']);
//Actualiza un registro
Route::put('Subcategorias/{id}',['uses'=>'SubcategoriasController@update']);







Route::get('datos',['uses'=> 'datos@index']);
Route::get('datos/{id}',['uses'=>'datos@show']);
Route::get('Myreference/{id}',['uses'=>'datos@misReferenciados']);
Route::post('datos',['uses'=>'datos@store']);
Route::put('datos/{id}',['uses'=>'datos@update']);
Route::delete('datos/{id}',['uses'=>'datos@destroy']);


Route::delete('DataBittrex/{id}',['uses'=>'datos@MyDataApiBittrex']);

Route::get('paises',['uses'=> 'datos@paises']);



Route::post('inscripcion',['uses'=> 'CursosController@store']);







Route::get('referidos/{id}',['uses'=> 'UsersController@index']);
Route::get('roles/{id}',['uses'=> 'UsersController@showrol']);

Route::get('moduls',['uses'=> 'UsersController@modulos']);

Route::post('roladd',['uses'=> 'UsersController@addrol']);
Route::delete('dltrol/{id}',['uses'=> 'UsersController@destroy']);


Route::put('editrol/{id}',['uses'=> 'UsersController@updaterol']);



Route::get('auth/{id}',['uses'=> 'UsersController@show']);
Route::get('tokenUser/{id}',['uses'=> 'UsersController@getProfile']);
Route::post('authtoken',['uses'=> 'UsersController@referenciado']);
Route::get('billetera',['uses'=> 'UsersController@billetera']);

Route::post('auth',['uses'=> 'UsersController@store']);


Route::post('login',['uses'=> 'UsersController@userAuth']);



Route::put('auth/{id}',['uses'=> 'UsersController@update']);
Route::delete('auth/{id}',['uses'=> 'UsersController@destroy']);



Route::post('modRol',['uses'=>'Asociate@create_relation']);
Route::get('modRol',['uses'=>'Asociate@index']);
Route::get('modRol/{id}',['uses'=>'Asociate@show']);
// Route::get('prf/{id}',['uses'=>'UsersController@getProfile']);
Route::get('confirmaccess/{token_key}',['uses'=>'UsersController@confirm']);





Route::get('proof',['uses'=>'UsersController@proof']);



Route::get('usuarios',['uses'=>'Mensajes@usuarios']);
Route::post('send',['uses'=>'Mensajes@sendEmail']);
Route::get('inbox/{id}',['uses'=>'Mensajes@index']);
Route::get('detailemail/{id}',['uses'=>'Mensajes@emaildetail']);

Route::get('notread/{id}',['uses'=>'Mensajes@MensajeNotLeidos']);
Route::get('leido/{id}',['uses'=>'Mensajes@marcarLeid']);