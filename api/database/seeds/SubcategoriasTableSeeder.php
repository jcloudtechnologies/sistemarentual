<?php

use Illuminate\Database\Seeder;

class SubcategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subcategorias')->insert([
            'categoria_id' => rand(1,10),
            'denominacion' => 'Cat. '.str_random(10),
            'active' => true,
        ]);
    }
}

