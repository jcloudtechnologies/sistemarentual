<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategorias extends Model
{


	protected $table= 'subcategorias';

	protected $fillable = [
	    'denominacion',
	];


	protected $hidden = ['created_at', 'updated_at'];
     

	public function data(){

	    return $this->belongsTo('App\Categorias','categoria_id');
	
	}

}
