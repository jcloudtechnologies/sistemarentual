<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Users;
use \App\module;

use App\Http\Controllers\Controller;
use JWTAuth;

class UsersController extends Controller
{
     
    public function userAuth(Request $request){


            // $credencials = $request->only('email','password');

           // var_dump('lo q trae angular',json_decode($request->input()));

            $credencials = $request->json()->all();
            $usr = Users::where('email',$credencials['email'])->first();
            // $result = $usr->confirm_email;

            // if (!$usr->confirm_email) {
            //     return response()->json([
            //         'msg'=>'Ingresa a tu Email y confirmalo',
            //         'status'=>'Error',
            //         'code'=>'500'
            //         ],500);
            // }
            $cred= array(
                'email' => $credencials['email'],
                'password'=>$credencials['password']);

            $token = null;

            try
            {

                if(!$token = JWTAuth::attempt($credencials)){
                    return response()->json([
                        'msg'=>'Token_Not_Created',
                        'status'=>'Error',
                        'code'=> 500,


                    ],500);
                }
                


            }catch(Exception $e)
            {
                return response()->json([
                    'msg'=>'Autenticacion erronea',
                    'status'=>'Error',
                    'data'=>$e->getMessage(),
                    'code' =>500,
                ],500);

            }


            $values = JWTAuth::toUser($token);
            return response()->json([

                'msg'=>'Session created',
                'status'=>'Successfull',
                'token'=>$token,
                'user'=> $values,
                'code'=>200

                ],200);

    }




}
