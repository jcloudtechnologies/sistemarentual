<?php

namespace App\Http\Controllers;

use App\Subcategorias;
use Illuminate\Http\Request;

class SubcategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        $data = Subcategorias::with('data')->get();

        

       return response()->json([
           'msg'=>'Todas las categorias',                
           'datos'=>$data->toArray(),
           'code'=>200
       ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request){

        $data = new Subcategorias();

        $denm = $request->json()->all();


        $data->denominacion = $denm['denominacion'];
        $data->categoria_id = $denm['categoria_id'];

                

        if ($data->save()) {
                   
            return response()->json([
             'msg'=>'Succesfull data save',
             'code'=>200
            ], 200);
                    
        }else{

            return response()->json(['msg'=>'Succesfull data dont save', 'code'=>500], 500);
                
        }
    }

    /**
     * Edit a register
     */
    public function edit($id){

        $data = Subcategorias::with('data')->find($id);

        return response()->json([
            'msg'=>'Editando sucategorias',            
            'datos'=>$data->toArray(),
            'code'=>200
            ],200);    

    }


    /**
     * Update a register
     */
    public function update($id, Request $request){



        $data = Subcategorias::find($id);

        $denm = $request->json()->all();

        $data->denominacion = $denm['denominacion'];
        $data->categoria_id = $denm['categoria_id'];


        if ($data->save()) {
           
            return response()->json(['msg'=>'Succesfull data save', 'code'=>200], 200);
            
        }else{

            return response()->json(['msg'=>'Succesfull data dont save', 'code'=>500], 500);
        
        }

        


    }


    /**
     * Delete
     */
    public function delete($id){

        $data = Subcategorias::find($id);    


        if ( $data->delete()) {
            return response()->json([
                'msg'=>'El registro fue eliminado',                
                'code'=>200
            ],200);  
             
         }else{

            return response()->json([
                'msg'=>'El registro no fue eliminado, por favor intente de nuevo.',                
                'code'=>500
            ],500);

         }


    }



}
