<?php

namespace App\Http\Controllers;

use App\Categorias;
use Illuminate\Http\Request;

class CategoriasController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data = Categorias::get();


        return response()->json([
            'msg'=>'Todas las categorias',                
            'datos'=>$data->toArray(),
            'code'=>200
        ],200);
    }


    /**
     * Add a new register
     */
    public function add(Request $request){

        $data = new Categorias();

        $denm = $request->json()->all();

        $data->denominacion = $denm['denominacion'];

        if ($data->save()) {
           
            return response()->json([
            'msg'=>'Succesfull data save',
             'code'=>200
             ], 200);
            
        }else{

            return response()->json(['msg'=>'Succesfull data dont save', 'code'=>500], 500);
        
        }

    }


    /**
     * Edit a register
     */
    public function edit($id){

        $data = Categorias::find($id);

        return response()->json([
            'msg'=>'Editando usuarios',            
            'datos'=>$data->toArray(),
            'code'=>200
            ],200);    

    }


    /**
     * Update a register
     */
    public function update($id,Request $request){



        $data = Categorias::find($id);


        $denominacion = $request->json()->all();

        $data->denominacion = $denominacion['denominacion'];


        if ($data->save()) {
           
            return response()->json(['msg'=>'Succesfull data save', 'code'=>200], 200);
            
        }else{

            return response()->json(['msg'=>'Succesfull data dont save', 'code'=>500], 500);
        
        }

        


    }



    /**
     * Delete
     */
    public function delete($id){

        $data = Categorias::find($id);    


        if ( $data->delete()) {
            return response()->json([
                'msg'=>'El registro fue eliminado',                
                'code'=>200
            ],200);  
             
         }else{

            return response()->json([
                'msg'=>'El registro no fue eliminado, por favor intente de nuevo.',                
                'code'=>500
            ],500);

         }


    }


}