<?php

namespace App\Http\Middleware;

use Closure;

class VerifyAccessKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
        public function handle($request, Closure $next)
    {
        // Obtenemos el api-key que el usuario envia
        $key = $request->headers->get('api_key');

        // Si coincide con el valor almacenado en la aplicacion
        // la aplicacion se sigue ejecutando
        if ($key == env('VRF_KEY')) {

            // return response()->json(['wtf'=> $key, 'lalmacenado'=>env('VRF_KEY')]);

            return $next($request);
        } else {
            // Si falla devolvemos el mensaje de error
            return response()->json([
                'msg'=>'No Autorizado- Token Api_key Requerido',
                'code'=>401
                ], 401);
        }
    }
}