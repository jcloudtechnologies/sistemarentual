<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;


class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        try{

            $user = JWTAuth::toUser($request->input('token'));


        }catch(JWTException $ex){

            if ($ex instanceof \Tymon\JWTAuth\Exception\TokenExpiredException) {
                return responce()->json([
                        'msg'=> 'Token Expired',
                        'data'=>$ex->getStatusCode()
                        'code'=> 500
                    ]);
                # code...
            }else if ($ex instanceof \Tymon\JWTAuth\Exception\TokenInvalidException) {
                return responce()->json([

                        'msg'=>'Token invalid',
                        'data'=>$ex->getStatusCode()
                        'code'=>500

                    ]);
            }else {
                return responce()->json([

                    'msg'=>'Token required',
                    'code'=>500


                    ]);                
            }

        }



        return $next($request);
    }
}
