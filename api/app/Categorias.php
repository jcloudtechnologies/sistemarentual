<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
 //
 protected $table= 'categorias';


 protected $fillable = [
     'denominacion',
 ];

 /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
 protected $hidden = [];





}
