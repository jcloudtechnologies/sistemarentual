function opendialog() {
    $("#dialog-message").dialog({modal: true, closeOnEscape: false});
}
function destroydialog() {
    $("#dialog-message").dialog("close");
}

$(document).ready(function() {

    //si existe el formulario de búsqueda
    if ($("#myModalmns")) {
        $("#vaciar_form_b").click(function() {
            $("#vaciar_form_").click();
        });

        $("#buscar_form_b").click(function() {
            $("#buscar_form_").click();
        });
    }

    //idiomas del date picker
    jQuery(function($) {
        $.datepicker.regional['es'] = {
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'Sabado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi�', 'Juv', 'Vie', 'Sab'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            dateFormat: 'dd/mm/yy'};
        $.datepicker.setDefaults($.datepicker.regional['es']);
    });

    //jquery validator, agregando comportamientos por default si se ha cargado el plug in
    if ($.validator) {
        $.validator.setDefaults({
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
                $(element).closest('.form-group').addClass('error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
                $(element).closest('.form-group').removeClass('text-danger');
                $(element).closest('.form-group').removeClass('error');
                $(element).closest('.form-group').find('.text-danger').css("display", "none");
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                if (!confirm("Esta seguro de procesar estos datos ??")) {
                    return false;
                } else {
                    $(window).unbind('beforeunload');
                    form.submit();
                }
            },
            invalidHandler: function(event, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $("#alerta-error").modal('show')
                } else {
                    $("div.error").hide();
                }
            },
            onkeyup: true
        });

        //jquery validator, agregando mensajes por default
        jQuery.extend(jQuery.validator.messages, {
            required: "Éste campo no puede estar vacío.",
            remote: "Please fix this field.",
            email: "Especifique una direccion de correo válida",
            url: "Especifique una direccion URL válida",
            date: "Especifique una fecha válida",
            dateISO: "Please enter a valid date (ISO).",
            number: "Especifique un número válido",
            digits: "Sólo se permiten números.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Por favor, repita el mismo valor.",
            accept: "Por favor, ingrese una extensión de archivo válida.",
            maxlength: jQuery.validator.format("No más de {0} caracter(es)."),
            minlength: jQuery.validator.format("No menos de {0} caracter(es)."),
            rangelength: jQuery.validator.format("El número de caracteres debe estar entre {0} y {1} caracteres."),
            range: jQuery.validator.format("Ingrese un valor entre {0} y {1}."),
            max: jQuery.validator.format("Ingrese un valor menor o igual a {0}."),
            min: jQuery.validator.format("Ingrese un valor mayor o igual a {0}.")
        });

        //bloqueando pagina si el usuario escribe o modifica el valor al menos de un campo.
        $(":input").bind("change", function() {
            $(window).bind('beforeunload', function() {
                return 'Tienes cambios no guardados. Si abandonas la página, éstos cambios se perderán.';
            });
        });

    }//fin si se ha cargado el validador

    if ($.fn.mask) { //jquery maskedinput, agregando comportamientos por default si se ha cargado el plug in
        $(".telefono").mask("(9999) 999-9999");
        $(".fecha").mask("99/99/9999");
    }

    $(".fecha").datepicker({
        yearRange: '-0:+5',
        changeMonth: true,
        changeYear: true,
        showOn: "both",
        buttonImage: "../img/calendar.png"});

    //agrega tooltips a todos los elementos con el atributo title definido
//    $('[title]').tooltip();
});