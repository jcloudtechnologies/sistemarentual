import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', 
              '../../assets/adminlte/plugins/iCheck/square/blue.css', 
              '../../assets/adminlte/dist/css/AdminLTE.min.css'
             ]
})

export class LoginComponent {
  title = 'app';
}
