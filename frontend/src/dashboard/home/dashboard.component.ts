import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css', 
              '../../assets/adminlte/dist/css/skins/_all-skins.min.css', 
              '../../assets/adminlte/dist/css/AdminLTE.min.css',
              '../../assets/adminlte/dist/css/font-awesome.min.css',
              '../../assets/adminlte/dist/css/ionicons.min.css',
              '../../assets/adminlte/bootstrap/css/bootstrap.min.css'
             ]
})

export class DashboardComponent {
  title = 'app';
}
