import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {RouterModule} from "@angular/router";

import { AppComponent } from './app.component';
import { AppFooterComponent } from '../footer/app-footer.component';
import { AppNavComponent } from '../nav/app-nav.component';
import { AppContactoComponent } from '../contacto/contacto.component';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../dashboard/login/login.component';
import { DashboardComponent } from '../dashboard/home/dashboard.component';
import { routing, appRoutingProviders } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    AppFooterComponent,
    AppNavComponent,
    AppContactoComponent, 
    HomeComponent,
    LoginComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpModule,
    routing,
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
