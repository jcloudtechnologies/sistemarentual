import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppContactoComponent } from '../contacto/contacto.component';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../dashboard/login/login.component';
import { DashboardComponent } from '../dashboard/home/dashboard.component';



const appRoutes:Routes=[
        {path:'',component:HomeComponent},
        {path:'contacto',component:AppContactoComponent},
        {path:'login',component:LoginComponent},
        {path:'dashboard',component:DashboardComponent}
        
];

export const appRoutingProviders:any[]=[];
export const routing: ModuleWithProviders= RouterModule.forRoot(appRoutes);

