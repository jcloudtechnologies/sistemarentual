import { Component, OnInit } from '@angular/core';
declare var jQuery;

@Component({
  selector: 'home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  title = 'app';


  ngOnInit(){

    //flexslider
    if (jQuery().flexslider) {
        //var $mainSlider = jQuery('#mainslider');
        jQuery("#mainslider .flexslider").flexslider({
            animation: "fade",
            useCSS: true,
            controlNav: true,
            directionNav: false,
            prevText: "",
            nextText: "",
            smoothHeight: false,
            slideshowSpeed: 8000,
            animationSpeed: 300,
            start: function (slider) {
                slider.find('.slide_description').children().css({'visibility': 'hidden'});
                slider.find('.flex-active-slide .slide_description').children().each(function (index) {
                    var self = jQuery(this);
                    var animationClass = !self.data('animation') ? 'fadeInRight' : self.data('animation');
                    setTimeout(function () {
                        self.addClass("animated " + animationClass);
                    }, index * 200);
                });
            },
            after: function (slider) {
                slider.find('.flex-active-slide .slide_description').children().each(function (index) {
                    var self = jQuery(this);
                    var animationClass = !self.data('animation') ? 'fadeInRight' : self.data('animation');
                    setTimeout(function () {
                        self.addClass("animated " + animationClass);
                    }, index * 200);
                });
            },
            end: function (slider) {
                slider.find('.slide_description').children().each(function () {
                    jQuery(this).attr('class', '');
                });
            }
        });

        jQuery(".flexslider").flexslider({
            animation: "fade",
            useCSS: true,
            controlNav: true,
            directionNav: false,
            prevText: "",
            nextText: "",
            //animationLoop: false,
            smoothHeight: true,
            slideshowSpeed: 5000,
            animationSpeed: 800,
            after: function (slider) {
                //console.log(slider.find('.slide_description').children());
                //bg-color1 - class for #mainslider
                //var currentClass = $mainSlider.find('.flex-active-slide').attr('data-bg');
                //$mainSlider.attr('class', currentClass);
            }
        });
    }

  }


}
