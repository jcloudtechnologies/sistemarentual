import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './layouts/login/login.component';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';

import { NavComponent } from './layouts/nav/nav.component';
import { MenuComponent } from './layouts/menu/menu.component';

import { CategoriaIndexComponent } from './components/categoria/index.component';
import { CategoriaAddComponent } from './components/categoria/add.component';
import { CategoriaEditComponent } from './components/categoria/edit.component';
import { CategoriaViewComponent } from './components/categoria/view.component';

import { CategoriaSubIndexComponent } from './components/categoriasub/index.component';
import { CategoriaSubAddComponent }   from './components/categoriasub/add.component';
import { CategoriaSubEditComponent }  from './components/categoriasub/edit.component';
import { CategoriaSubViewComponent }  from './components/categoriasub/view.component';

const appRoutes:Routes=[
        {path:'',component:LoginComponent},
        {path:'login',component:LoginComponent},
        {path:'dashboard',component:DashboardComponent,
                children:[
                    {path:'categoria',component:CategoriaIndexComponent},
                    {path:'categoria/add',component:CategoriaAddComponent},
                    {path:'categoria/edit/:id',component:CategoriaEditComponent},
                    {path:'categoria/view/:id',component:CategoriaViewComponent},

                    {path:'categoriasub',component:CategoriaSubIndexComponent},
                    {path:'categoriasub/add',component:CategoriaSubAddComponent},
                    {path:'categoriasub/edit/:id',component:CategoriaSubEditComponent},
                    {path:'categoriasub/view/:id',component:CategoriaSubViewComponent},
                ]
        },
        
];

export const appRoutingProviders:any[]=[];
export const routing: ModuleWithProviders= RouterModule.forRoot(appRoutes);

