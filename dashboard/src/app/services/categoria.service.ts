import { Injectable } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

import { Categoria } from '../models/categoria.modelo';

@Injectable()
export class CategoriaService{
        public url:string;
        public v:string;
        public headers = new Headers();
                constructor(
                public _http:Http
                ){
                    this.url="http://sistemarentual.jcloudtec.com/engine/public/api/";
                    this.headers.append('Content-Type','application/x-www-form-urlencoded');
                    this.headers.append('api_key','$2y$10$CZ3XgjF5GCAYXkIOXyGesOqnaiNYUNX8XK7KOHwwW9AF5attbBKWe');
                }

        getAll(){
                return this._http.get(this.url+'Categorias',{headers:this.headers})
                .map(res=>res.json());
        }
        
        getId(id){
                return this._http.get(this.url+'Categorias/'+id,{headers:this.headers})
                .map(res=>res.json());
        }

        postAdd(datos:Categoria){
                return this._http.post(this.url+'Categorias',
                JSON.stringify({
                'denominacion':datos.denominacion,
                })
                ,{headers:this.headers}).map(res=>res.json());
        }

        deleteId(id){
                return this._http.delete(this.url+'Categorias/'+id,{headers:this.headers})
                .map(res=>res.json());
        }

        putAdd(id,datos:Categoria){
                return this._http.put(this.url+'Categorias/'+id,
                JSON.stringify({
                'denominacion':datos.denominacion,
                })
                ,{headers:this.headers}).map(res=>res.json());
        }



}