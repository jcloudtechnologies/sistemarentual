import { Injectable } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

import { Login } from '../models/login.modelo';

@Injectable()
export class StrService{
        public url:string;
        public v:string;
        public headers = new Headers();
                constructor(
                public _http:Http
                ){
                    this.url="http://sistemarentual.jcloudtec.com/engine/public/api/";
                    this.headers.append('Content-Type','application/x-www-form-urlencoded');
                    this.headers.append('api_key','$2y$10$CZ3XgjF5GCAYXkIOXyGesOqnaiNYUNX8XK7KOHwwW9AF5attbBKWe');
                }
        LoginAuth(Login:Login){
                return this._http.post(this.url+"login",
                        JSON.stringify({
                        'email': Login.username,
                        'password':Login.password})
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
}