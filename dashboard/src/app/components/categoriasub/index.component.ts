import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//import { Categoria } from '../../models/categoria.modelo';
import { CategoriaService } from '../../services/categoria.service';

import { Subcategoria } from '../../models/subcategoria.modelo';
import { SubcategoriaService } from '../../services/categoriasub.service';

import { Subject } from 'rxjs/Rx';
declare var alertify;
declare var jQuery;

@Component({
  selector: 'app-categoriasub-index',
  templateUrl: './index.component.html',
  providers:[CategoriaService, SubcategoriaService]
})
export class CategoriaSubIndexComponent  implements OnInit{
   title = 'Sub Categorias';
  // public Categoria:Categoria;
   public Subcategoria:Subcategoria;
   public data:any=[];
   dtOptions: DataTables.Settings = {};
   dtTrigger: Subject<any> = new Subject();
   constructor(
                private _router: Router,
                private _route: ActivatedRoute,
                private _service: SubcategoriaService
              ){
                this.Subcategoria= new Subcategoria('', '');
    }
    ngOnInit(){
		    if (!sessionStorage.getItem('tokensession')){
				this._router.navigate(['login']);
			}   
			this.dtOptions = {
				destroy:true,
				pagingType: 'full_numbers',
				pageLength: 10,
				search:true,
				paging:true,
			};
			this._service.getAll().subscribe(response =>{
				if (response.code!=200){
					//this.data=arr;
					//alert();
				}else{
					this.data = response.datos;
				   // alert(JSON.stringify(this.data));
					this.dtTrigger.next();
				}
			},
			error=>{

			});
    }

	public confirmado;

	borrarConfirm(id){
		this.confirmado = id;
	}

	cancelarConfirm(){
		this.confirmado = null;
	}

	onDeleteId(id){
		this._service.deleteId(id).subscribe(
			response => {
				if (response.code == 200) {
					 //this._router.navigate(['dashboard/categoria']);
          			 jQuery('#datatable').DataTable().row('#tr_'+id).remove().draw(false); 
                     alertify.success("Los datos fueron eliminados"); 
				} else {
					 alertify.error("Los datos no fueron eliminados");
				}
			},
			error => {
				alertify.error("Los datos no fueron eliminados");
				console.log(<any>error);
			}
		);
	}


    /*Datatable(dateset){
		    alert(dateset);
			jQuery('#data').DataTable({
				dom: 'Bfrtlip',
				data: dateset,
				responsive: true,
				columns: [
					{ "data":"denominacion" }
				],

				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				],
				"language": 
				{
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ registros",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
			} );
    }*/
}
