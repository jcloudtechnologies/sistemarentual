import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CategoriaService } from '../../services/categoria.service';
import { Subcategoria } from '../../models/subcategoria.modelo';
import { SubcategoriaService } from '../../services/categoriasub.service';
import { Subject } from 'rxjs/Rx';
declare var alertify;

@Component({
  selector: 'app-categoriasub-edit',
  templateUrl: './edit.component.html',
  providers:[CategoriaService, SubcategoriaService]
})
export class CategoriaSubEditComponent  implements OnInit{
  title = 'Sub Categorias';
  public subcategoria:Subcategoria;
  public data:any=[];
  public categorias:any=[];
  constructor(
              private _router: Router,
              private _route: ActivatedRoute,
              private _service: SubcategoriaService,
              private _servicecategoria: CategoriaService,
              ){
              this.subcategoria= new Subcategoria('', '');
  }

  ngOnInit(){
              if (!sessionStorage.getItem('tokensession')){
                    this._router.navigate(['login']);
              }
              this._servicecategoria.getAll().subscribe(response =>{
                  if (response.code!=200){
                  }else{
                    this.categorias = response.datos;
                  }
                },
                error=>{
              });
              this._route.params.forEach((params:Params)=>{
                  let id = params['id'];
                  this._service.getId(id).subscribe(
                    response=>{
                      if (response.code==200){
                        this.subcategoria = response.datos;
                      }else{
                        console.log('error en el response');
                      }
                    },error=>{
                        console.log('Error '+<any>error);
                    }
                    );
                });
                
  }
  onSubmit(){
              this._route.params.forEach((params:Params)=>{
                  let id = params['id'];
                  this._service.putAdd(id, this.subcategoria).subscribe(
                  response =>{
                                if (response.code!=200){
                                    alertify.error("Los datos no fueron gurdados");
                                }else{
                                    this._router.navigate(['dashboard/categoriasub']); 
                                    alertify.success("Los datos fuero guardados"); 
                                }
                          },
                          error =>{
                                    console.log(<any>error);
                                    alertify.error("Los datos no fueron guardados");
                          }
                  );
                }); 
  }

  
}//fin
