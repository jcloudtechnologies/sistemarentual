import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CategoriaService } from '../../services/categoria.service';
import { Subcategoria } from '../../models/subcategoria.modelo';
import { SubcategoriaService } from '../../services/categoriasub.service';
import { Subject } from 'rxjs/Rx';
declare var alertify;

@Component({
  selector: 'app-categoriasub-add',
  templateUrl: './add.component.html',
  providers:[CategoriaService, SubcategoriaService]
})
export class CategoriaSubAddComponent  implements OnInit{
  title = 'Sub Categorias';
  public subcategoria:Subcategoria;
  public categorias:any=[];
  constructor(
              private _router: Router,
              private _route: ActivatedRoute,
              private _service: SubcategoriaService,
              private _servicecategoria: CategoriaService,
              ){
              this.subcategoria= new Subcategoria('', '');
  }

  ngOnInit(){
    if (!sessionStorage.getItem('tokensession')){
          this._router.navigate(['login']);
    }
    this._servicecategoria.getAll().subscribe(response =>{
				if (response.code!=200){
				}else{
					this.categorias = response.datos;
				}
			},
			error=>{
			});
  }
  onSubmit(){
            this._service.postAdd(this.subcategoria).subscribe(
            response =>{
                          if (response.code!=200){
                               alertify.error("Los datos no fueron guardados");
                          }else{
                               this._router.navigate(['dashboard/categoriasub']); 
                               alertify.success("Los datos fueron guardados"); 
                          }
                    },
                    error =>{
                              alertify.error("Los datos no fueron guardados");
                              console.log(<any>error);
                    }
            );
  }

  
}
