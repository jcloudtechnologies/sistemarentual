import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CategoriaService } from '../../services/categoria.service';
import { Subcategoria } from '../../models/subcategoria.modelo';
import { SubcategoriaService } from '../../services/categoriasub.service';
import { Subject } from 'rxjs/Rx';
declare var alertify;

@Component({
  selector: 'app-categoriasub-view',
  templateUrl: './view.component.html',
  providers:[CategoriaService, SubcategoriaService]
})
export class CategoriaSubViewComponent  implements OnInit{
  title = 'Sub Categorias';
  public subcategoria:Subcategoria;
  public data:any=[];
  constructor(
              private _router: Router,
              private _route: ActivatedRoute,
              private _service: SubcategoriaService
              ){
              this.subcategoria= new Subcategoria('', '');
  }

  ngOnInit(){
              if (!sessionStorage.getItem('tokensession')){
                    this._router.navigate(['login']);
              }
              this._route.params.forEach((params:Params)=>{
                  let id = params['id'];
                  this._service.getId(id).subscribe(
                    response=>{
                      if (response.code=200)
                      {
                        
                        this.data = response.datos;
                       // alert(JSON.stringify(this.data));
                      }else{
                        console.log('error en el response');
                      }
                    },error=>{
                        console.log('Error '+<any>error);
                    }
                    );
                });
  }

  
}//fin
