import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Categoria } from '../../models/categoria.modelo';
import { CategoriaService } from '../../services/categoria.service';
import { Subject } from 'rxjs/Rx';
declare var alertify;

@Component({
  selector: 'app-categoria-edit',
  templateUrl: './edit.component.html',
  providers:[CategoriaService]
})
export class CategoriaEditComponent  implements OnInit{
  title = 'Categorias';
  public categoria:Categoria;
  public data:any=[];
  constructor(
              private _router: Router,
              private _route: ActivatedRoute,
              private _service: CategoriaService
              ){
              this.categoria= new Categoria('');
  }

  ngOnInit(){
              if (!sessionStorage.getItem('tokensession')){
                    this._router.navigate(['login']);
              }
              this._route.params.forEach((params:Params)=>{
                  let id = params['id'];
                  this._service.getId(id).subscribe(
                    response=>{
                      if (response.code==200){
                        this.categoria = response.datos;
                      }else{
                        console.log('error en el response');
                      }
                    },error=>{
                        console.log('Error '+<any>error);
                    }
                    );
                });
  }
  onSubmit(){
              this._route.params.forEach((params:Params)=>{
                  let id = params['id'];
                  this._service.putAdd(id, this.categoria).subscribe(
                  response =>{
                                if (response.code!=200){
                                    alertify.error("Los datos no fueron gurdados");
                                }else{
                                    this._router.navigate(['dashboard/categoria']); 
                                    alertify.success("Los datos fuero guardados"); 
                                }
                          },
                          error =>{
                                    console.log(<any>error);
                                    alertify.error("Los datos no fueron guardados");
                          }
                  );
                }); 
  }

  
}//fin
