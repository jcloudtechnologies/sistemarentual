import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Categoria } from '../../models/categoria.modelo';
import { CategoriaService } from '../../services/categoria.service';
import { Subject } from 'rxjs/Rx';
declare var alertify;

@Component({
  selector: 'app-categoria-view',
  templateUrl: './view.component.html',
  providers:[CategoriaService]
})
export class CategoriaViewComponent  implements OnInit{
  title = 'Categorias';
  public categoria:Categoria;
  public data:any=[];
  constructor(
              private _router: Router,
              private _route: ActivatedRoute,
              private _service: CategoriaService
              ){
              this.categoria= new Categoria('');
  }

  ngOnInit(){
              if (!sessionStorage.getItem('tokensession')){
                    this._router.navigate(['login']);
              }
              this._route.params.forEach((params:Params)=>{
                  let id = params['id'];
                  this._service.getId(id).subscribe(
                    response=>{
                      if (response.code=200)
                      {
                        this.data = response.datos;
                      }else{
                        console.log('error en el response');
                      }
                    },error=>{
                        console.log('Error '+<any>error);
                    }
                    );
                });
  }

  
}//fin
