import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Categoria } from '../../models/categoria.modelo';
import { CategoriaService } from '../../services/categoria.service';
import { Subject } from 'rxjs/Rx';
declare var alertify;

@Component({
  selector: 'app-categoria-add',
  templateUrl: './add.component.html',
  providers:[CategoriaService]
})
export class CategoriaAddComponent  implements OnInit{
  title = 'Categorias';
  public categoria:Categoria;
  constructor(
              private _router: Router,
              private _route: ActivatedRoute,
              private _service: CategoriaService
              ){
              this.categoria= new Categoria('');
  }

  ngOnInit(){
    if (!sessionStorage.getItem('tokensession')){
          this._router.navigate(['login']);
    }
  }
  onSubmit(){
            this._service.postAdd(this.categoria).subscribe(
            response =>{
                          if (response.code!=200){
                               alertify.error("Los datos no fueron guardados");
                          }else{
                               this._router.navigate(['dashboard/categoria']); 
                               alertify.success("Los datos fueron guardados"); 
                          }
                    },
                    error =>{
                              alertify.error("Los datos no fueron guardados");
                              console.log(<any>error);
                    }
            );
  }

  
}
