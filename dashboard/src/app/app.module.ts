import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { Observable }  from 'rxjs/Observable'; 

import { DataTablesModule } from 'angular-datatables'; 

import { routing, appRoutingProviders } from './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './layouts/login/login.component';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';
import { MenuComponent } from './layouts/menu/menu.component';
import { NavComponent } from './layouts/nav/nav.component';

import { CategoriaIndexComponent } from './components/categoria/index.component';
import { CategoriaAddComponent } from './components/categoria/add.component';
import { CategoriaEditComponent } from './components/categoria/edit.component';
import { CategoriaViewComponent } from './components/categoria/view.component';


import { CategoriaSubIndexComponent } from './components/categoriasub/index.component';
import { CategoriaSubAddComponent } from './components/categoriasub/add.component';
import { CategoriaSubEditComponent } from './components/categoriasub/edit.component';
import { CategoriaSubViewComponent } from './components/categoriasub/view.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NavComponent,
    MenuComponent,
    CategoriaIndexComponent,
    CategoriaAddComponent,
    CategoriaEditComponent,
    CategoriaViewComponent,

    CategoriaSubIndexComponent,
    CategoriaSubAddComponent,
    CategoriaSubEditComponent,
    CategoriaSubViewComponent
    
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpModule,
    routing,
    FormsModule,
    DataTablesModule
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule{}
