import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {
  title = 'app';
  constructor(
          private _router: Router,
          private _route: ActivatedRoute
          ){}
  ngOnInit(){
    if (!sessionStorage.getItem('tokensession')){
          this._router.navigate(['login']);
    }
  }
}
