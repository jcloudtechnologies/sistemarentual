import { Component } from '@angular/core';

@Component({
  selector: 'dashboard-nav',
  templateUrl: './nav.component.html'
})
export class NavComponent {
  title = 'app';

  sessionClosed(){
      sessionStorage.removeItem('tokensession') ;
      sessionStorage.removeItem('user') ;
      sessionStorage.removeItem('messages');

  }
}
