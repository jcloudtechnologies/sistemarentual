import { Component } from '@angular/core';

@Component({
  selector: 'dashboard-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent {
  title = 'app';
}
