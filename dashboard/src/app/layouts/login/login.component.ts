import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Login } from '../../models/login.modelo';
import { StrService } from '../../services/login.service';
declare var alertify;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers:[StrService]
})
export class LoginComponent implements OnInit{
        public login:Login;
        public rsp : any = [];
        constructor(
                private _router: Router,
                private _route: ActivatedRoute,
                private _service: StrService
                ){
                this.login= new Login('','')
        }
        ngOnInit(){
        console.log('login component initial');
          if (sessionStorage.getItem('tokensession')){
                  this._router.navigate(['starterview']);
          }
        }
        onSubmit(){
        this._service.LoginAuth(this.login).subscribe(
                        response =>{
                                          if (response.code!=200){
                                              alertify.error("Datos incorreptos");
                                              console.log(JSON.stringify(response.msg));
                                          }else{
                                              sessionStorage.setItem('tokensession',response.token);
                                              sessionStorage.setItem('user',response.user.name);
                                              sessionStorage.setItem('user_id',response.user.id);
                                              // console.log('Session Created');
                                              this._router.navigate(['dashboard']);
                                          }
                                    },
                                    error =>{
                                            this.rsp = error.json();
                                            alertify.error("Datos incorreptos");
                                            //alert(JSON.stringify(this.rsp.msg));

                                    }
        );
        }
}
